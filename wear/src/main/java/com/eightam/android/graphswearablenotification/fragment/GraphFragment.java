package com.eightam.android.graphswearablenotification.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eightam.android.graphs.helper.GraphPointHelper;
import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphs.point.GraphPoint;
import com.eightam.android.graphs.widget.GraphView;
import com.eightam.android.graphs.widget.GraphViewFactory;
import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableFragmentComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableActivityComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;

import java.lang.ref.SoftReference;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GraphFragment extends BaseFragment implements
        ValuesStorage.Listener,
        SettingsStorage.Listener {

    @Inject
    ValuesStorage mValuesStorage;

    @Inject
    SettingsStorage mSettingsStorage;

    @Bind(R.id.text_title)
    TextView mTextTitle;

    @Bind(R.id.graph_container)
    ViewGroup mGraphContainer;

    SoftReference<Listener> mListenerRef;

    public static GraphFragment newInstance(Listener listener) {
        GraphFragment fragment = new GraphFragment();
        fragment.mListenerRef = new SoftReference<>(listener);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_graph, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(GraphFragment.this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDi();

        updateUi();
    }

    @Override
    public void onStart() {
        super.onStart();
        registerStorageListeners();
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterStorageListeners();
    }

    @Override
    public void onValuesChanged(ValuesStorage valuesStorage) {
        updateUi();
    }

    @Override
    public void onSettingsChanged(SettingsStorage settingsStorage) {
        updateUi();
    }

    public void setListener(Listener listener) {
        mListenerRef = new SoftReference<>(listener);
    }

    private void initializeDi() {
        WearableActivityComponent wearableActivityComponent = getComponent(WearableActivityComponent.class);

        DaggerWearableFragmentComponent.builder()
                .wearableActivityComponent(wearableActivityComponent)
                .storageModule(new StorageModule())
                .build()
                .inject(GraphFragment.this);
    }

    private void registerStorageListeners() {
        mValuesStorage.registerListener(GraphFragment.this);
        mSettingsStorage.registerListener(GraphFragment.this);
    }

    private void unregisterStorageListeners() {
        mValuesStorage.unregisterListener(GraphFragment.this);
        mSettingsStorage.unregisterListener(GraphFragment.this);
    }

    private void updateUi() {
        float[] values = mValuesStorage.getValues();
        GraphType graphType = mSettingsStorage.getGraphType();

        if (values.length > 0) {
            List<GraphPoint> points = GraphPointHelper.toPoints(values);
            GraphView graphView = GraphViewFactory.createGraphView(getActivity(), graphType, points);
            graphView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> ((GraphView) v).startAnimation());
            graphView.setOnClickListener((view) -> notifyListenerOnGraphClicked());

            mTextTitle.setText(getActivity().getResources().getString(R.string.value, values[0]));

            mGraphContainer.removeAllViews();
            mGraphContainer.addView(graphView, new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
    }

    private void notifyListenerOnGraphClicked() {
        Listener listener = mListenerRef.get();

        if (listener != null) {
            listener.onGraphClicked();
        }
    }

    public interface Listener {

        void onGraphClicked();

    }

}
