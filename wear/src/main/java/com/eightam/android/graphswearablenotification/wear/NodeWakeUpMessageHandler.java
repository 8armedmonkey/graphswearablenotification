package com.eightam.android.graphswearablenotification.wear;

import android.content.Context;
import android.os.PowerManager;
import android.os.Vibrator;

import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableApplicationServiceHandlerComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableApplicationServiceComponent;
import com.google.android.gms.wearable.MessageEvent;

import javax.inject.Inject;

public class NodeWakeUpMessageHandler extends BaseWearableListenerServiceMessageHandler {

    @Inject
    Context mContext;

    public NodeWakeUpMessageHandler(WearableApplicationServiceComponent wearableApplicationServiceComponent) {
        super(wearableApplicationServiceComponent);
    }

    @Override
    public void handle(MessageEvent messageEvent) {
        PowerManager powerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);

        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                        | PowerManager.FULL_WAKE_LOCK
                        | PowerManager.ACQUIRE_CAUSES_WAKEUP, K.wakelock_tag.WAKE_LOCK);

        wakeLock.acquire(K.app.WAKE_LOCK_TIMEOUT_MILLIS);
        vibrator.vibrate(K.app.WAKE_LOCK_TIMEOUT_MILLIS);
    }

    @Override
    protected void initializeDi() {
        DaggerWearableApplicationServiceHandlerComponent.builder()
                .wearableApplicationServiceComponent(mWearableApplicationServiceComponent)
                .build()
                .inject(NodeWakeUpMessageHandler.this);
    }

}
