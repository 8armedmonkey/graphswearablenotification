package com.eightam.android.graphswearablenotification.wear;

import com.eightam.android.graphswearablenotification.internal.di.component.WearableApplicationServiceComponent;

public abstract class BaseWearableListenerServiceMessageHandler implements MessageHandler {

    protected WearableApplicationServiceComponent mWearableApplicationServiceComponent;

    public BaseWearableListenerServiceMessageHandler(WearableApplicationServiceComponent wearableApplicationServiceComponent) {
        mWearableApplicationServiceComponent = wearableApplicationServiceComponent;
        initializeDi();
    }

    protected abstract void initializeDi();

}
