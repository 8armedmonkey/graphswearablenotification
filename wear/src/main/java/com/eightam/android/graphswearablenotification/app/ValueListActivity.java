package com.eightam.android.graphswearablenotification.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;

import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.fragment.ValueListFragment;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageEvent;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ValueListActivity extends BaseWearableActivity implements WatchViewStub.OnLayoutInflatedListener {

    @Bind(R.id.watch_view_stub)
    WatchViewStub mWatchViewStub;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, ValueListActivity.class);
    }

    @Override
    public void onLayoutInflated(WatchViewStub watchViewStub) {
        showValueList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(ValueListActivity.this);

        mWatchViewStub.setOnLayoutInflatedListener(ValueListActivity.this);
    }

    private void showValueList() {
        ValueListFragment valueListFragment =
                (ValueListFragment) getSupportFragmentManager().findFragmentByTag(K.fragment_tag.VALUE_LIST);

        if (valueListFragment == null) {
            valueListFragment = ValueListFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, valueListFragment, K.fragment_tag.VALUE_LIST)
                    .commit();
        }
    }

}
