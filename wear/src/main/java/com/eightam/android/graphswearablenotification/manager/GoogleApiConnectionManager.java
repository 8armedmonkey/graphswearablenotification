package com.eightam.android.graphswearablenotification.manager;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

import java.lang.ref.SoftReference;

public class GoogleApiConnectionManager implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private SoftReference<Activity> mActivityRef;
    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError;
    private SoftReference<Listener> mListenerRef;

    public GoogleApiConnectionManager(Activity activity, Listener listener, ApiOptionsPair... apiOptionsPairs) {
        mActivityRef = new SoftReference<>(activity);
        mListenerRef = new SoftReference<>(listener);
        initializeGoogleApiClient(apiOptionsPairs);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Listener listener = mListenerRef.get();

        if (listener != null) {
            listener.onConnected(connectionHint);
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Listener listener = mListenerRef.get();

        if (listener != null) {
            listener.onConnectionSuspended(cause);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Listener listener = mListenerRef.get();

        if (listener != null) {
            listener.onConnectionFailedWithNoResolution(result);
        }
    }

    public void onActivityStart() {
        if (!mResolvingError) {
            mGoogleApiClient.connect();
        }
    }

    public void onActivityStop() {
        mGoogleApiClient.disconnect();
    }

    public void connectGoogleApiClientIfNeeded() {
        if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    public boolean isResolvingError() {
        return mResolvingError;
    }

    public void setResolvingError(boolean resolvingError) {
        mResolvingError = resolvingError;
    }

    public void setListener(Listener listener) {
        mListenerRef = new SoftReference<>(listener);
    }

    private void initializeGoogleApiClient(ApiOptionsPair... apiOptionsPairs) {
        Activity activity = mActivityRef.get();
        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(GoogleApiConnectionManager.this)
                .addOnConnectionFailedListener(GoogleApiConnectionManager.this);

        for (ApiOptionsPair apiOptionsPair : apiOptionsPairs) {
            if (apiOptionsPair.mOptions != null) {
                builder.addApi(apiOptionsPair.mApi, apiOptionsPair.mOptions);
            } else {
                builder.addApi(apiOptionsPair.mApi);
            }
        }

        mGoogleApiClient = builder.build();
    }

    public interface Listener {

        void onConnected(Bundle connectionHint);

        void onConnectionSuspended(int cause);

        void onConnectionFailedWithNoResolution(ConnectionResult result);

    }

    public static class ApiOptionsPair {

        Api mApi;
        Api.ApiOptions.HasOptions mOptions;

        public ApiOptionsPair(Api api) {
            mApi = api;
        }

        public ApiOptionsPair(Api api, Api.ApiOptions.HasOptions options) {
            mApi = api;
            mOptions = options;
        }

    }

}
