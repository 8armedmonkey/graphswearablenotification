package com.eightam.android.graphswearablenotification.internal.di.component;

import android.app.Activity;
import android.content.Context;

import com.eightam.android.graphswearablenotification.app.BaseWearableActivity;
import com.eightam.android.graphswearablenotification.common.internal.di.annotation.ActivityScope;
import com.eightam.android.graphswearablenotification.internal.di.module.WearableActivityModule;
import com.eightam.android.graphswearablenotification.manager.GoogleApiConnectionManager;
import com.eightam.android.graphswearablenotification.manager.SyncManager;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.google.android.gms.common.api.GoogleApiClient;

import dagger.Component;

@Component(modules = {WearableActivityModule.class})
@ActivityScope
public interface WearableActivityComponent {

    void inject(BaseWearableActivity baseWearableActivity);

    Activity activity();

    Context activityContext();

    GoogleApiConnectionManager.Listener googleApiConnectionManagerListener();

    GoogleApiConnectionManager googleApiConnectionManager();

    GoogleApiClient googleApiClient();

    WearableCommunicationManager wearableCommunicationManager();

    SyncManager syncManager();

}
