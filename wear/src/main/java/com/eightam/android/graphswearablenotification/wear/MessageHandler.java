package com.eightam.android.graphswearablenotification.wear;

import com.google.android.gms.wearable.MessageEvent;

public interface MessageHandler {

    void handle(MessageEvent messageEvent);

}
