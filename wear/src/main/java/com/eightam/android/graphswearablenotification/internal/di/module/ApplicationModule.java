package com.eightam.android.graphswearablenotification.internal.di.module;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    public Context provideAppContext() {
        return mApplication;
    }

    @Provides
    public Resources provideResources() {
        return mApplication.getResources();
    }

}
