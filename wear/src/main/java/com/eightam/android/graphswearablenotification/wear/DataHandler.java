package com.eightam.android.graphswearablenotification.wear;

import com.google.android.gms.wearable.DataEvent;

public interface DataHandler {

    void handle(DataEvent dataEvent);

}
