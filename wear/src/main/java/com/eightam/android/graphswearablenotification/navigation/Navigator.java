package com.eightam.android.graphswearablenotification.navigation;

import android.content.Context;

import com.eightam.android.graphswearablenotification.app.ValueListActivity;

import javax.inject.Inject;

public class Navigator {

    private Context mContext;

    @Inject
    public Navigator(Context context) {
        mContext = context;
    }

    public void goToValueList() {
        mContext.startActivity(ValueListActivity.getStartIntent(mContext));
    }

}
