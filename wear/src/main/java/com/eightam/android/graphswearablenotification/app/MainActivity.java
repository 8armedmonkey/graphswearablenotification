package com.eightam.android.graphswearablenotification.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.wearable.view.WatchViewStub;

import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.fragment.GraphFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseWearableActivity implements
        WatchViewStub.OnLayoutInflatedListener,
        GraphFragment.Listener {

    private static final String EXTRA_HAS_REQUESTED_SYNC = "hasRequestedSync";

    boolean mHasRequestedSync;

    @Bind(R.id.watch_view_stub)
    WatchViewStub mWatchViewStub;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

        if (K.fragment_tag.GRAPH.equals(fragment.getTag()) &&
                fragment instanceof GraphFragment) {
            ((GraphFragment) fragment).setListener(MainActivity.this);
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);

        if (!mHasRequestedSync) {
            requestSync();
            mHasRequestedSync = true;
        }
    }

    @Override
    public void onLayoutInflated(WatchViewStub watchViewStub) {
        showGraph();
    }

    @Override
    public void onGraphClicked() {
        goToValueList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(MainActivity.this);

        if (savedInstanceState != null) {
            mHasRequestedSync = savedInstanceState.getBoolean(EXTRA_HAS_REQUESTED_SYNC);
        }

        mWatchViewStub.setOnLayoutInflatedListener(MainActivity.this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_HAS_REQUESTED_SYNC, mHasRequestedSync);
    }

    private void requestSync() {
        mSyncManager.sync();
    }

    private void showGraph() {
        GraphFragment graphFragment =
                (GraphFragment) getSupportFragmentManager().findFragmentByTag(K.fragment_tag.GRAPH);

        if (graphFragment == null) {
            graphFragment = GraphFragment.newInstance(MainActivity.this);

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, graphFragment, K.fragment_tag.GRAPH)
                    .commit();
        }
    }

    private void goToValueList() {
        mNavigator.goToValueList();
    }

}
