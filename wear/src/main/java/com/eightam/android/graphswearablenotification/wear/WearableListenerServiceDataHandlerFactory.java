package com.eightam.android.graphswearablenotification.wear;

import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableApplicationServiceComponent;

public class WearableListenerServiceDataHandlerFactory {

    public static DataHandler createDataHandler(WearableApplicationServiceComponent wearableApplicationServiceComponent,
                                                String path,
                                                String action) {
        if (Data.path.SYNC.equals(path) && Data.action.SYNC.equals(action)) {
            return new SyncDataHandler(wearableApplicationServiceComponent);
        }

        throw new IllegalArgumentException("There is no data handler for the path / action.");
    }

}
