package com.eightam.android.graphswearablenotification.wear;

import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableApplicationServiceComponent;

public class WearableListenerServiceMessageHandlerFactory {

    public static MessageHandler createMessageHandler(WearableApplicationServiceComponent wearableApplicationServiceComponent,
                                                      String path,
                                                      String action) {
        if (Data.path.NODE.equals(path) && Data.action.WAKE_UP.equals(action)) {
            return new NodeWakeUpMessageHandler(wearableApplicationServiceComponent);
        }

        throw new IllegalArgumentException("There is no message handler for the path / action.");
    }

}
