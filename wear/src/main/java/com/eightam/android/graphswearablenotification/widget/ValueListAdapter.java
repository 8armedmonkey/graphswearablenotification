package com.eightam.android.graphswearablenotification.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eightam.android.graphswearablenotification.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ValueListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private float[] mValues;

    public ValueListAdapter(@NonNull Context context, @NonNull float[] values) {
        mContext = context;
        mValues = values;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_value, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).mTextTitle.setText(mContext.getResources().getString(R.string.value, mValues[position]));
    }

    @Override
    public int getItemCount() {
        return mValues.length;
    }

    @NonNull
    public float[] getValues() {
        return mValues;
    }

    public void setValues(@NonNull float[] values) {
        mValues = values;
        notifyDataSetChanged();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.text_title)
        TextView mTextTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(ItemViewHolder.this, itemView);
        }

    }

}
