package com.eightam.android.graphswearablenotification.internal.di.component;

import com.eightam.android.graphswearablenotification.internal.di.annotation.WearableApplicationServiceHandlerScope;
import com.eightam.android.graphswearablenotification.wear.NodeWakeUpMessageHandler;
import com.eightam.android.graphswearablenotification.wear.SyncDataHandler;

import dagger.Component;

@Component(dependencies = {WearableApplicationServiceComponent.class})
@WearableApplicationServiceHandlerScope
public interface WearableApplicationServiceHandlerComponent extends WearableApplicationServiceComponent {

    void inject(SyncDataHandler syncDataHandler);

    void inject(NodeWakeUpMessageHandler nodeWakeUpMessageHandler);

}
