package com.eightam.android.graphswearablenotification.wear;

import com.eightam.android.graphswearablenotification.internal.di.component.WearableApplicationServiceComponent;

public abstract class BaseWearableListenerServiceDataHandler implements DataHandler {

    protected WearableApplicationServiceComponent mWearableApplicationServiceComponent;

    public BaseWearableListenerServiceDataHandler(WearableApplicationServiceComponent wearableApplicationServiceComponent) {
        mWearableApplicationServiceComponent = wearableApplicationServiceComponent;
        initializeDi();
    }

    protected abstract void initializeDi();

}
