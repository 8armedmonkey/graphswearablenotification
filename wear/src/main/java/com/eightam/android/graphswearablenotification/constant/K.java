package com.eightam.android.graphswearablenotification.constant;

public interface K {

    interface app {

        long WAKE_LOCK_TIMEOUT_MILLIS = 2000;

    }

    interface fragment_tag {

        String GRAPH = "graph";
        String VALUE_LIST = "valueList";

    }

    interface wakelock_tag {

        String WAKE_LOCK = "wakeLock";

    }

}
