package com.eightam.android.graphswearablenotification.internal.di.module;

import android.content.Context;

import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.SharedPreferencesSettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.SharedPreferencesValuesStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    public ValuesStorage provideValuesStorage(Context context) {
        return new SharedPreferencesValuesStorage(context);
    }

    @Provides
    public SettingsStorage provideSettingsStorage(Context context) {
        return new SharedPreferencesSettingsStorage(context);
    }

}
