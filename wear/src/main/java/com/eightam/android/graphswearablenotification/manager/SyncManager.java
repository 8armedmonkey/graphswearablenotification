package com.eightam.android.graphswearablenotification.manager;

import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.common.messaging.MessageData;
import com.eightam.android.graphswearablenotification.common.messaging.MessageHelper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.Iterator;

public class SyncManager implements ResultCallback<CapabilityApi.GetCapabilityResult> {

    private GoogleApiClient mGoogleApiClient;
    private WearableCommunicationManager mWearableCommunicationManager;
    private boolean mInitializingSync;
    private boolean mCancelled;

    public SyncManager(GoogleApiClient googleApiClient, WearableCommunicationManager wearableCommunicationManager) {
        mGoogleApiClient = googleApiClient;
        mWearableCommunicationManager = wearableCommunicationManager;
    }

    @Override
    public void onResult(CapabilityApi.GetCapabilityResult getCapabilityResult) {
        if (!mCancelled) {
            CapabilityInfo capabilityInfo = getCapabilityResult.getCapability();

            if (capabilityInfo.getNodes().size() > 0) {
                for (Node node : capabilityInfo.getNodes()) {
                    if (node.isNearby()) {
                        MessageData messageData = new MessageData();
                        messageData.setAction(Data.action.SYNC);

                        mWearableCommunicationManager.sendMessage(node.getId(), Data.path.SYNC, MessageHelper.toBytes(messageData));
                    }
                }
            }
        }

        mInitializingSync = false;
        mCancelled = false;
    }

    public void sync() {
        synchronized (SyncManager.this) {
            if (!mInitializingSync) {
                assertGoogleApiClientConnected();

                mInitializingSync = true;
                mCancelled = false;

                PendingResult<CapabilityApi.GetCapabilityResult> pendingResult =
                        Wearable.CapabilityApi.getCapability(mGoogleApiClient, Data.capability.SYNC, CapabilityApi.FILTER_REACHABLE);
                pendingResult.setResultCallback(SyncManager.this);
            }
        }
    }

    public void cancel() {
        synchronized (SyncManager.this) {
            mCancelled = true;
        }
    }

    public boolean isInitializingSync() {
        return mInitializingSync;
    }

    public boolean isCancelled() {
        return mCancelled;
    }

    private void assertGoogleApiClientConnected() {
        if (!mGoogleApiClient.isConnected()) {
            throw new IllegalStateException("GoogleApiClient is not connected");
        }
    }

}
