package com.eightam.android.graphswearablenotification.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableFragmentComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableActivityComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.widget.ValueListAdapter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ValueListFragment extends BaseFragment implements ValuesStorage.Listener {

    @Inject
    ValuesStorage mValuesStorage;

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    public static ValueListFragment newInstance() {
        return new ValueListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_value_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(ValueListFragment.this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDi();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(new ValueListAdapter(getActivity(), mValuesStorage.getValues()));
    }

    @Override
    public void onStart() {
        super.onStart();
        registerStorageListeners();
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterStorageListeners();
    }

    @Override
    public void onValuesChanged(ValuesStorage valuesStorage) {
        ((ValueListAdapter) mRecyclerView.getAdapter()).setValues(mValuesStorage.getValues());
    }

    private void initializeDi() {
        WearableActivityComponent wearableActivityComponent = getComponent(WearableActivityComponent.class);

        DaggerWearableFragmentComponent.builder()
                .wearableActivityComponent(wearableActivityComponent)
                .storageModule(new StorageModule())
                .build()
                .inject(ValueListFragment.this);
    }

    private void registerStorageListeners() {
        mValuesStorage.registerListener(ValueListFragment.this);
    }

    private void unregisterStorageListeners() {
        mValuesStorage.unregisterListener(ValueListFragment.this);
    }

}
