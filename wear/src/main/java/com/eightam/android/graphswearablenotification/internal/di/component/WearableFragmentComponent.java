package com.eightam.android.graphswearablenotification.internal.di.component;

import android.support.v4.app.Fragment;

import com.eightam.android.graphswearablenotification.common.internal.di.annotation.FragmentScope;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.fragment.GraphFragment;
import com.eightam.android.graphswearablenotification.fragment.ValueListFragment;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;

import dagger.Component;

@Component(dependencies = {WearableActivityComponent.class}, modules = {StorageModule.class})
@FragmentScope
public interface WearableFragmentComponent extends WearableActivityComponent {

    void inject(GraphFragment graphFragment);

    void inject(ValueListFragment valueListFragment);

    ValuesStorage valuesStorage();

    SettingsStorage settingsStorage();

}
