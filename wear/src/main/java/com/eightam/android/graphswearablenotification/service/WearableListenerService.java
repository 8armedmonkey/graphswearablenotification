package com.eightam.android.graphswearablenotification.service;

import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.common.messaging.MessageData;
import com.eightam.android.graphswearablenotification.common.messaging.MessageHelper;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableApplicationServiceComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableApplicationServiceComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.ApplicationServiceModule;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.internal.di.module.WearableModule;
import com.eightam.android.graphswearablenotification.wear.WearableListenerServiceDataHandlerFactory;
import com.eightam.android.graphswearablenotification.wear.WearableListenerServiceMessageHandlerFactory;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;

import javax.inject.Inject;

public class WearableListenerService extends com.google.android.gms.wearable.WearableListenerService {

    WearableApplicationServiceComponent mWearableApplicationServiceComponent;

    @Inject
    ValuesStorage mValuesStorage;

    @Inject
    SettingsStorage mSettingsStorage;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDi();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        super.onDataChanged(dataEvents);

        for (DataEvent dataEvent : dataEvents) {
            DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
            DataMap dataMap = dataMapItem.getDataMap();

            String path = dataEvent.getDataItem().getUri().getPath();
            String action = dataMap.getString(Data.key.ACTION);

            WearableListenerServiceDataHandlerFactory
                    .createDataHandler(mWearableApplicationServiceComponent, path, action)
                    .handle(dataEvent);
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);

        MessageData messageData = MessageHelper.toMessageData(messageEvent.getData());

        String path = messageEvent.getPath();
        String action = messageData.getAction();

        WearableListenerServiceMessageHandlerFactory
                .createMessageHandler(mWearableApplicationServiceComponent, path, action)
                .handle(messageEvent);
    }

    private void initializeDi() {
        mWearableApplicationServiceComponent = DaggerWearableApplicationServiceComponent.builder()
                .applicationServiceModule(new ApplicationServiceModule(WearableListenerService.this))
                .wearableModule(new WearableModule())
                .storageModule(new StorageModule())
                .build();

        mWearableApplicationServiceComponent.inject(WearableListenerService.this);
    }

}
