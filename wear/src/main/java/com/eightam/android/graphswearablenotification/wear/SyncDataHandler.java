package com.eightam.android.graphswearablenotification.wear;

import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableApplicationServiceHandlerComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableApplicationServiceComponent;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;

import javax.inject.Inject;

public class SyncDataHandler extends BaseWearableListenerServiceDataHandler {

    @Inject
    ValuesStorage mValuesStorage;

    @Inject
    SettingsStorage mSettingsStorage;

    public SyncDataHandler(WearableApplicationServiceComponent wearableApplicationServiceComponent) {
        super(wearableApplicationServiceComponent);
    }

    @Override
    public void handle(DataEvent dataEvent) {
        DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
        DataMap dataMap = dataMapItem.getDataMap();

        float[] values = dataMap.getFloatArray(Data.key.VALUES);
        String graphTypeAsString = dataMap.getString(Data.key.GRAPH_TYPE);

        mValuesStorage.setValues(values);
        mSettingsStorage.setGraphType(GraphType.valueOf(graphTypeAsString.toUpperCase()));
    }

    @Override
    protected void initializeDi() {
        DaggerWearableApplicationServiceHandlerComponent.builder()
                .wearableApplicationServiceComponent(mWearableApplicationServiceComponent)
                .build()
                .inject(SyncDataHandler.this);
    }

}
