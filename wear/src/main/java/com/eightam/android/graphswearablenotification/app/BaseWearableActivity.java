package com.eightam.android.graphswearablenotification.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.eightam.android.graphswearablenotification.common.internal.di.annotation.HasComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableActivityComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableActivityComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.WearableActivityModule;
import com.eightam.android.graphswearablenotification.manager.GoogleApiConnectionManager;
import com.eightam.android.graphswearablenotification.manager.SyncManager;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.eightam.android.graphswearablenotification.navigation.Navigator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;

import javax.inject.Inject;

public abstract class BaseWearableActivity extends AppCompatActivity implements
        HasComponent<WearableActivityComponent>,
        GoogleApiConnectionManager.Listener,
        DataApi.DataListener,
        MessageApi.MessageListener {

    protected WearableActivityComponent mWearableActivityComponent;

    @Inject
    protected Navigator mNavigator;

    @Inject
    protected GoogleApiConnectionManager mGoogleApiConnectionManager;

    @Inject
    protected WearableCommunicationManager mWearableCommunicationManager;

    @Inject
    protected SyncManager mSyncManager;

    @Override
    public WearableActivityComponent getComponent() {
        return mWearableActivityComponent;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        GoogleApiClient googleApiClient = mGoogleApiConnectionManager.getGoogleApiClient();

        Wearable.DataApi.addListener(googleApiClient, BaseWearableActivity.this);
        Wearable.MessageApi.addListener(googleApiClient, BaseWearableActivity.this);
    }

    @Override
    public void onConnectionSuspended(int cause) {
    }

    @Override
    public void onConnectionFailedWithNoResolution(ConnectionResult result) {
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiConnectionManager.onActivityStart();
    }

    @Override
    protected void onStop() {
        GoogleApiClient googleApiClient = mGoogleApiConnectionManager.getGoogleApiClient();

        if (googleApiClient.isConnected()) {
            Wearable.DataApi.removeListener(googleApiClient, BaseWearableActivity.this);
            Wearable.MessageApi.removeListener(googleApiClient, BaseWearableActivity.this);
        }

        if (mSyncManager.isInitializingSync()) {
            mSyncManager.cancel();
        }

        mGoogleApiConnectionManager.onActivityStop();
        super.onStop();
    }

    protected void initializeDi() {
        mWearableActivityComponent = DaggerWearableActivityComponent.builder()
                .wearableActivityModule(new WearableActivityModule(BaseWearableActivity.this))
                .build();

        mWearableActivityComponent.inject(BaseWearableActivity.this);
    }

}
