package com.eightam.android.graphswearablenotification.fragment;

import android.support.v4.app.Fragment;

import com.eightam.android.graphswearablenotification.common.internal.di.annotation.HasComponent;

public abstract class BaseFragment extends Fragment {

    @SuppressWarnings("unchecked")
    protected <T> T getComponent(Class<T> componentType) {
        return componentType.cast(((HasComponent<T>) getActivity()).getComponent());
    }

}
