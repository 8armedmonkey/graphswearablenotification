package com.eightam.android.graphswearablenotification.manager;

import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.service.ValuesService;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

public class ValuesUpdateManager {

    private ValuesService mValuesService;
    private ValuesStorage mValuesStorage;

    @Inject
    public ValuesUpdateManager(ValuesService valuesService, ValuesStorage valuesStorage) {
        mValuesService = valuesService;
        mValuesStorage = valuesStorage;
    }

    public Observable<float[]> updateValues() {
        return mValuesService.getValuesStream()
                .flatMap(saveValues());
    }

    private Func1<List<Float>, Observable<float[]>> saveValues() {
        return (values) -> {
            float[] valuesAsPrimitives = new float[values.size()];

            for (int i = 0, n = values.size(); i < n; i++) {
                valuesAsPrimitives[i] = values.get(i);
            }

            mValuesStorage.setValues(valuesAsPrimitives);

            return Observable.just(valuesAsPrimitives);
        };
    }

}
