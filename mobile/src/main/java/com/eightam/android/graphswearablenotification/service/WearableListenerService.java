package com.eightam.android.graphswearablenotification.service;

import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.common.messaging.MessageData;
import com.eightam.android.graphswearablenotification.common.messaging.MessageHelper;
import com.google.android.gms.wearable.MessageEvent;

public class WearableListenerService extends com.google.android.gms.wearable.WearableListenerService {

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);

        if (Data.path.SYNC.equals(messageEvent.getPath())) {
            MessageData messageData = MessageHelper.toMessageData(messageEvent.getData());

            if (Data.action.SYNC.equals(messageData.getAction())) {
                startService(ValuesUpdaterService.getStartIntent(WearableListenerService.this));
            }
        }
    }

}
