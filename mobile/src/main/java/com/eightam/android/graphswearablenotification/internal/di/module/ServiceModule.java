package com.eightam.android.graphswearablenotification.internal.di.module;

import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.manager.ValuesUpdateManager;
import com.eightam.service.RandomValuesService;
import com.eightam.service.ValuesService;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    @Provides
    public ValuesService provideValuesService() {
        return new RandomValuesService(5, -100.0f, 100.0f);
    }

    @Provides
    public ValuesUpdateManager provideValuesUpdateManager(ValuesService valuesService, ValuesStorage valuesStorage) {
        return new ValuesUpdateManager(valuesService, valuesStorage);
    }

}
