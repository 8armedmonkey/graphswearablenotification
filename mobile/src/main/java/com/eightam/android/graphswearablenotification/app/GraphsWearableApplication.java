package com.eightam.android.graphswearablenotification.app;

import android.app.Application;

import com.eightam.android.graphswearablenotification.R;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParsePush;

public class GraphsWearableApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
    }

    private void initialize() {
        String parseApplicationId = getResources().getString(R.string.parse_app_id);
        String parseClientKey = getResources().getString(R.string.parse_client_key);

        Parse.initialize(GraphsWearableApplication.this, parseApplicationId, parseClientKey);
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

}
