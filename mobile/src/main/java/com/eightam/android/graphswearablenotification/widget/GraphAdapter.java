package com.eightam.android.graphswearablenotification.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eightam.android.graphs.helper.GraphPointHelper;
import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphs.widget.GraphView;
import com.eightam.android.graphs.widget.GraphViewFactory;
import com.eightam.android.graphswearablenotification.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GraphAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_GRAPH = 1;
    private static final int VIEW_TYPE_VALUE = 2;

    private Context mContext;
    private GraphType mGraphType;
    private float[] mValues;
    private boolean mGraphHasBeenAnimated;

    public GraphAdapter(@NonNull Context context, @NonNull GraphType graphType, @NonNull float[] values) {
        mContext = context;
        mGraphType = graphType;
        mValues = values;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_GRAPH: {
                return new GraphViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_graph_container, parent, false));
            }

            case VIEW_TYPE_VALUE: {
                return new ValueViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_value, parent, false));
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof GraphViewHolder) {
            onBindGraphViewHolder((GraphViewHolder) holder, position);

        } else if (holder instanceof ValueViewHolder) {
            onBindValueViewHolder((ValueViewHolder) holder, position);

        }
    }

    @Override
    public int getItemCount() {
        return mValues.length + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItemCount() > 0) {
            return position == 0 ? VIEW_TYPE_GRAPH : VIEW_TYPE_VALUE;
        }
        return super.getItemViewType(position);
    }

    @NonNull
    public GraphType getGraphType() {
        return mGraphType;
    }

    public void setGraphType(GraphType graphType) {
        mGraphType = graphType;
        notifyDataSetChanged();
    }

    @NonNull
    public float[] getValues() {
        return mValues;
    }

    public void setValues(@NonNull float[] values) {
        mValues = values;
        mGraphHasBeenAnimated = false;
        notifyDataSetChanged();
    }

    private void onBindGraphViewHolder(GraphViewHolder holder, int position) {
        GraphView graphView = GraphViewFactory.createGraphView(holder.itemView.getContext(), mGraphType, GraphPointHelper.toPoints(mValues));
        graphView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> ((GraphView) v).startAnimation());

        if (mGraphHasBeenAnimated) {
            graphView.setAnimationDurationMillis(0);
        }

        holder.mGraphContainer.removeAllViews();
        holder.mGraphContainer.addView(graphView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    private void onBindValueViewHolder(ValueViewHolder holder, int position) {
        holder.mTextTitle.setText(mContext.getResources().getString(R.string.value, mValues[position - 1]));
    }

    static class GraphViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.graph_container)
        ViewGroup mGraphContainer;

        public GraphViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(GraphViewHolder.this, itemView);
        }

    }

    static class ValueViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.text_title)
        TextView mTextTitle;

        public ValueViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(ValueViewHolder.this, itemView);
        }

    }

}
