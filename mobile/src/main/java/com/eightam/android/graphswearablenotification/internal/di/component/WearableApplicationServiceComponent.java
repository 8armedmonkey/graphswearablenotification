package com.eightam.android.graphswearablenotification.internal.di.component;

import android.content.Context;
import android.content.res.Resources;

import com.eightam.android.graphswearablenotification.common.internal.di.annotation.ServiceScope;
import com.eightam.android.graphswearablenotification.internal.di.module.ApplicationServiceModule;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.internal.di.module.ServiceModule;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.internal.di.module.WearableModule;
import com.eightam.android.graphswearablenotification.manager.ValuesUpdateManager;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.eightam.android.graphswearablenotification.service.ValuesUpdaterService;
import com.eightam.android.graphswearablenotification.service.WearableListenerService;
import com.eightam.android.graphswearablenotification.service.WearablePushHandlerService;
import com.eightam.android.graphswearablenotification.service.WearableWakeUpService;
import com.eightam.service.ValuesService;
import com.google.android.gms.common.api.GoogleApiClient;

import dagger.Component;

@Component(modules = {ApplicationServiceModule.class, WearableModule.class, ServiceModule.class, StorageModule.class})
@ServiceScope
public interface WearableApplicationServiceComponent {

    void inject(ValuesUpdaterService valuesUpdaterService);

    void inject(WearableListenerService wearableListenerService);

    void inject(WearablePushHandlerService wearablePushHandlerService);

    void inject(WearableWakeUpService wearableWakeUpService);

    Context serviceContext();

    Resources resources();

    GoogleApiClient googleApiClient();

    WearableCommunicationManager wearableCommunicationManager();

    ValuesService valuesService();

    ValuesUpdateManager valuesUpdateManager();

    ValuesStorage valuesStorage();

    SettingsStorage settingsStorage();

}
