package com.eightam.android.graphswearablenotification.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.google.android.gms.common.GooglePlayServicesUtil;

import java.lang.ref.SoftReference;

public class GoogleApiConnectionErrorDialogFragment extends DialogFragment {

    private int mErrorCode;
    private int mRequestCode;
    private SoftReference<OnDismissListener> mOnDismissListenerRef;

    public static GoogleApiConnectionErrorDialogFragment newInstance(int errorCode, int requestCode, OnDismissListener onDismissListener) {
        GoogleApiConnectionErrorDialogFragment fragment = new GoogleApiConnectionErrorDialogFragment();
        fragment.mErrorCode = errorCode;
        fragment.mRequestCode = requestCode;
        fragment.mOnDismissListenerRef = new SoftReference<>(onDismissListener);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return GooglePlayServicesUtil.getErrorDialog(mErrorCode, getActivity(), mRequestCode);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        notifyOnDismissListener(dialog);
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        mOnDismissListenerRef = new SoftReference<>(onDismissListener);
    }

    private void notifyOnDismissListener(DialogInterface dialog) {
        if (mOnDismissListenerRef != null) {
            OnDismissListener onDismissListener = mOnDismissListenerRef.get();

            if (onDismissListener != null) {
                onDismissListener.onDismiss(dialog);
            }
        }
    }

    public interface OnDismissListener {

        void onDismiss(DialogInterface dialog);

    }

}
