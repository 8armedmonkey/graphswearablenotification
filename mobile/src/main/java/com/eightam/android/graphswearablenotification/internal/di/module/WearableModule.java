package com.eightam.android.graphswearablenotification.internal.di.module;

import android.content.Context;

import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import dagger.Module;
import dagger.Provides;

@Module
public class WearableModule {

    private GoogleApiClient mGoogleApiClient;
    private WearableCommunicationManager mWearableCommunicationManager;

    @Provides
    public GoogleApiClient provideGoogleApiClient(Context context) {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(context).addApi(Wearable.API).build();
        }
        return mGoogleApiClient;
    }

    @Provides
    public WearableCommunicationManager provideWearableCommunicationManager(GoogleApiClient googleApiClient) {
        if (mWearableCommunicationManager == null) {
            mWearableCommunicationManager = new WearableCommunicationManager(googleApiClient);
        }
        return mWearableCommunicationManager;
    }

}
