package com.eightam.android.graphswearablenotification.internal.di.module;

import android.app.Service;
import android.content.Context;
import android.content.res.Resources;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationServiceModule {

    private Service mService;

    public ApplicationServiceModule(Service service) {
        mService = service;
    }

    @Provides
    public Context provideServiceContext() {
        return mService;
    }

    @Provides
    public Resources provideResources() {
        return mService.getResources();
    }

}
