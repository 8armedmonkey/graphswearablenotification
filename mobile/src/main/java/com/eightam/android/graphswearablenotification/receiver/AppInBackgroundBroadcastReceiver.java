package com.eightam.android.graphswearablenotification.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v7.app.NotificationCompat;

import com.eightam.android.graphs.helper.GraphPointHelper;
import com.eightam.android.graphs.widget.GraphView;
import com.eightam.android.graphs.widget.GraphViewFactory;
import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.app.MainActivity;
import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.service.WearableWakeUpService;

public class AppInBackgroundBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        // TODO Refactor.
        if (K.intent.action.FORWARD_BROADCAST_WITH_FOREGROUND_CHECK.equals(action)) {
            float[] values = intent.getFloatArrayExtra(Data.key.VALUES);

            String contentTitle = context.getResources().getString(R.string.notification_title_values_changed);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, MainActivity.getStartIntent(context), PendingIntent.FLAG_UPDATE_CURRENT);

            int bigPictureWidth = context.getResources().getDimensionPixelSize(R.dimen.notification_big_picture_width);
            int bigPictureHeight = context.getResources().getDimensionPixelSize(R.dimen.notification_big_picture_height);

            SettingsStorage settingsStorage = new StorageModule().provideSettingsStorage(context);

            GraphView graphView = GraphViewFactory.createGraphView(context, settingsStorage.getGraphType(), GraphPointHelper.toPoints(values));
            graphView.setAnimationDurationMillis(0);
            graphView.layout(0, 0, bigPictureWidth, bigPictureHeight);
            graphView.startAnimation();

            Bitmap bitmap = Bitmap.createBitmap(bigPictureWidth, bigPictureHeight, Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(bitmap);

            graphView.draw(canvas);

            NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
            bigPictureStyle.bigLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
            bigPictureStyle.bigPicture(bitmap);
            bigPictureStyle.setBigContentTitle(contentTitle);
            bigPictureStyle.setSummaryText(null);

            Notification notification = new NotificationCompat.Builder(context)
                    .setAutoCancel(true)
                    .setContentTitle(contentTitle)
                    .setContentIntent(contentIntent)
                    .setTicker(contentTitle)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setStyle(bigPictureStyle)
                    .setVibrate(new long[] {0, 1000, 500, 1000})
                    .build();

            NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender(notification);
            wearableExtender.setHintScreenTimeout(NotificationCompat.WearableExtender.SCREEN_TIMEOUT_LONG);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(K.notification.VALUES_CHANGED, notification);

            Intent serviceIntent = new Intent(context, WearableWakeUpService.class);
            startWakefulService(context, serviceIntent);

        } else {
            completeWakefulIntent(intent);
        }
    }

}
