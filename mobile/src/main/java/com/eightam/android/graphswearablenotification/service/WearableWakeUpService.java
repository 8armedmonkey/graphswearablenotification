package com.eightam.android.graphswearablenotification.service;

import android.app.IntentService;
import android.content.Intent;

import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.common.messaging.MessageData;
import com.eightam.android.graphswearablenotification.common.messaging.MessageHelper;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableApplicationServiceComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.ApplicationServiceModule;
import com.eightam.android.graphswearablenotification.internal.di.module.ServiceModule;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.internal.di.module.WearableModule;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.eightam.android.graphswearablenotification.receiver.AppInBackgroundBroadcastReceiver;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class WearableWakeUpService extends IntentService {

    @Inject
    GoogleApiClient mGoogleApiClient;

    @Inject
    WearableCommunicationManager mWearableCommunicationManager;

    public WearableWakeUpService() {
        super(WearableWakeUpService.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDi();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (connectGoogleApiClientIfNeeded()) {
            PendingResult<NodeApi.GetConnectedNodesResult> pendingResult = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient);
            NodeApi.GetConnectedNodesResult getConnectedNodesResult = pendingResult.await();

            for (Node node : getConnectedNodesResult.getNodes()) {
                MessageData messageData = new MessageData();
                messageData.setAction(Data.action.WAKE_UP);

                mWearableCommunicationManager.sendMessage(node.getId(), Data.path.NODE, MessageHelper.toBytes(messageData));
            }

            AppInBackgroundBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private void initializeDi() {
        DaggerWearableApplicationServiceComponent.builder()
                .applicationServiceModule(new ApplicationServiceModule(WearableWakeUpService.this))
                .wearableModule(new WearableModule())
                .serviceModule(new ServiceModule())
                .storageModule(new StorageModule())
                .build()
                .inject(WearableWakeUpService.this);
    }

    private boolean connectGoogleApiClientIfNeeded() {
        if (!mGoogleApiClient.isConnected()) {
            ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(
                    K.app.GOOGLE_API_CONNECTION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

            return connectionResult.isSuccess();
        }
        return true;
    }

}
