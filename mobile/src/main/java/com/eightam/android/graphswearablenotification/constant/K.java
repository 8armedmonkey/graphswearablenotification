package com.eightam.android.graphswearablenotification.constant;

public interface K {

    interface app {

        long GOOGLE_API_CONNECTION_TIMEOUT_MILLIS = 30000;

    }

    interface intent {

        interface action {

            String BASE = "com.eightam.android.graphswearablenotification.intent.action";
            String FORWARD_BROADCAST_WITH_FOREGROUND_CHECK = BASE + ".FORWARD_BROADCAST_WITH_FOREGROUND_CHECK";

        }

        interface request {

            int RESOLVE_GOOGLE_API_CONNECTION = 1000;

        }

    }

    interface fragment_tag {

        String GOOGLE_API_CONNECTION_ERROR_DIALOG = "googleApiConnectionErrorDialog";
        String GRAPH = "graph";
        String GRAPH_SELECTION = "graphSelection";

    }

    interface notification {

        int VALUES_CHANGED = 1000;

    }

    interface parse {

        String EXTRA_DATA = "com.parse.Data";

    }

}
