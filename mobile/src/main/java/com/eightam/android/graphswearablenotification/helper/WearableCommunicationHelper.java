package com.eightam.android.graphswearablenotification.helper;

import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.google.android.gms.wearable.PutDataMapRequest;

public class WearableCommunicationHelper {

    public static PutDataMapRequest createSyncRequest(float[] values, GraphType graphType) {
        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(Data.path.SYNC);
        putDataMapRequest.getDataMap().putString(Data.key.ACTION, Data.action.SYNC);
        putDataMapRequest.getDataMap().putFloatArray(Data.key.VALUES, values);
        putDataMapRequest.getDataMap().putString(Data.key.GRAPH_TYPE, graphType.toString());

        return putDataMapRequest;
    }

}
