package com.eightam.android.graphswearablenotification.app;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.eightam.android.graphswearablenotification.common.internal.di.annotation.HasComponent;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.fragment.GoogleApiConnectionErrorDialogFragment;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableActivityComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableActivityComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.WearableActivityModule;
import com.eightam.android.graphswearablenotification.manager.GoogleApiConnectionManager;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;

import javax.inject.Inject;

public abstract class BaseWearableActivity extends AppCompatActivity implements
        HasComponent<WearableActivityComponent>,
        GoogleApiConnectionManager.Listener,
        DataApi.DataListener,
        MessageApi.MessageListener,
        GoogleApiConnectionErrorDialogFragment.OnDismissListener {

    protected WearableActivityComponent mWearableActivityComponent;

    @Inject
    protected GoogleApiConnectionManager mGoogleApiConnectionManager;

    @Inject
    protected WearableCommunicationManager mWearableCommunicationManager;

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

        if (K.fragment_tag.GOOGLE_API_CONNECTION_ERROR_DIALOG.equals(fragment.getTag()) &&
                fragment instanceof GoogleApiConnectionErrorDialogFragment) {
            ((GoogleApiConnectionErrorDialogFragment) fragment).setOnDismissListener(BaseWearableActivity.this);
        }
    }

    @Override
    public WearableActivityComponent getComponent() {
        return mWearableActivityComponent;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        GoogleApiClient googleApiClient = mGoogleApiConnectionManager.getGoogleApiClient();

        Wearable.DataApi.addListener(googleApiClient, BaseWearableActivity.this);
        Wearable.MessageApi.addListener(googleApiClient, BaseWearableActivity.this);
    }

    @Override
    public void onConnectionSuspended(int cause) {
    }

    @Override
    public void onConnectionFailedWithNoResolution(ConnectionResult result) {
        showGoogleApiConnectionErrorDialog(result.getErrorCode());
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        mGoogleApiConnectionManager.setResolvingError(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiConnectionManager.onActivityStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case K.intent.request.RESOLVE_GOOGLE_API_CONNECTION: {
                mGoogleApiConnectionManager.setResolvingError(false);

                if (resultCode == RESULT_OK) {
                    mGoogleApiConnectionManager.connectGoogleApiClientIfNeeded();
                }
                break;
            }
        }
    }

    @Override
    protected void onStop() {
        GoogleApiClient googleApiClient = mGoogleApiConnectionManager.getGoogleApiClient();

        if (googleApiClient.isConnected()) {
            Wearable.DataApi.removeListener(googleApiClient, BaseWearableActivity.this);
            Wearable.MessageApi.removeListener(googleApiClient, BaseWearableActivity.this);
        }

        mGoogleApiConnectionManager.onActivityStop();
        super.onStop();
    }

    protected void initializeDi() {
        mWearableActivityComponent = DaggerWearableActivityComponent.builder()
                .wearableActivityModule(new WearableActivityModule(BaseWearableActivity.this))
                .build();

        mWearableActivityComponent.inject(BaseWearableActivity.this);
    }

    protected void showGoogleApiConnectionErrorDialog(int errorCode) {
        GoogleApiConnectionErrorDialogFragment googleApiConnectionErrorDialogFragment =
                (GoogleApiConnectionErrorDialogFragment) getSupportFragmentManager().findFragmentByTag(K.fragment_tag.GOOGLE_API_CONNECTION_ERROR_DIALOG);

        if (googleApiConnectionErrorDialogFragment == null) {
            googleApiConnectionErrorDialogFragment = GoogleApiConnectionErrorDialogFragment.newInstance(
                    errorCode, K.intent.request.RESOLVE_GOOGLE_API_CONNECTION, BaseWearableActivity.this);

            googleApiConnectionErrorDialogFragment.show(getSupportFragmentManager(), K.fragment_tag.GOOGLE_API_CONNECTION_ERROR_DIALOG);
        }
    }

}
