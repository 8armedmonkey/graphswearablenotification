package com.eightam.android.graphswearablenotification.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.eightam.android.graphswearablenotification.receiver.PushWakefulBroadcastReceiver;
import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.helper.WearableCommunicationHelper;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableApplicationServiceComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.ApplicationServiceModule;
import com.eightam.android.graphswearablenotification.internal.di.module.ServiceModule;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.internal.di.module.WearableModule;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.PutDataMapRequest;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class WearablePushHandlerService extends IntentService {

    @Inject
    GoogleApiClient mGoogleApiClient;

    @Inject
    WearableCommunicationManager mWearableCommunicationManager;

    @Inject
    ValuesStorage mValuesStorage;

    @Inject
    SettingsStorage mSettingsStorage;

    public WearablePushHandlerService() {
        super(WearablePushHandlerService.class.getName());
    }

    public static Intent getStartIntent(Context context, float[] values) {
        Intent intent = new Intent(context, WearablePushHandlerService.class);
        intent.putExtra(Data.key.VALUES, values);

        return intent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDi();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (connectGoogleApiClientIfNeeded()) {
            float[] values = intent.getFloatArrayExtra(Data.key.VALUES);

            mValuesStorage.setValues(values);

            PutDataMapRequest putDataMapRequest =
                    WearableCommunicationHelper.createSyncRequest(mValuesStorage.getValues(), mSettingsStorage.getGraphType());

            mWearableCommunicationManager.putDataItem(putDataMapRequest);

            Intent broadcastIntent = new Intent(K.intent.action.FORWARD_BROADCAST_WITH_FOREGROUND_CHECK);
            broadcastIntent.putExtra(Data.key.VALUES, values);

            sendOrderedBroadcast(broadcastIntent, null);
        }

        PushWakefulBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void initializeDi() {
        DaggerWearableApplicationServiceComponent.builder()
                .applicationServiceModule(new ApplicationServiceModule(WearablePushHandlerService.this))
                .wearableModule(new WearableModule())
                .serviceModule(new ServiceModule())
                .storageModule(new StorageModule())
                .build()
                .inject(WearablePushHandlerService.this);
    }

    private boolean connectGoogleApiClientIfNeeded() {
        if (!mGoogleApiClient.isConnected()) {
            ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(
                    K.app.GOOGLE_API_CONNECTION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

            return connectionResult.isSuccess();
        }
        return true;
    }

}
