package com.eightam.android.graphswearablenotification.internal.di.module;

import android.app.Activity;
import android.content.Context;

import com.eightam.android.graphswearablenotification.app.BaseWearableActivity;
import com.eightam.android.graphswearablenotification.manager.GoogleApiConnectionManager;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import dagger.Module;
import dagger.Provides;

@Module
public class WearableActivityModule {

    private BaseWearableActivity mBaseWearableActivity;
    private GoogleApiConnectionManager mGoogleApiConnectionManager;
    private WearableCommunicationManager mWearableCommunicationManager;

    public WearableActivityModule(BaseWearableActivity baseWearableActivity) {
        mBaseWearableActivity = baseWearableActivity;
    }

    @Provides
    public Activity provideActivity() {
        return mBaseWearableActivity;
    }

    @Provides
    public Context provideActivityContext() {
        return mBaseWearableActivity;
    }

    @Provides
    public GoogleApiConnectionManager.Listener provideGoogleApiConnectionManagerListener() {
        return mBaseWearableActivity;
    }

    @Provides
    public GoogleApiConnectionManager provideGoogleApiConnectionManager(Activity activity, GoogleApiConnectionManager.Listener listener) {
        if (mGoogleApiConnectionManager == null) {
            mGoogleApiConnectionManager = new GoogleApiConnectionManager(activity, listener, new GoogleApiConnectionManager.ApiOptionsPair(Wearable.API));
        }
        return mGoogleApiConnectionManager;
    }

    @Provides
    public GoogleApiClient provideGoogleApiClient(GoogleApiConnectionManager googleApiConnectionManager) {
        return googleApiConnectionManager.getGoogleApiClient();
    }

    @Provides
    public WearableCommunicationManager provideWearableCommunicationManager(GoogleApiClient googleApiClient) {
        if (mWearableCommunicationManager == null) {
            mWearableCommunicationManager = new WearableCommunicationManager(googleApiClient);
        }
        return mWearableCommunicationManager;
    }

}
