package com.eightam.android.graphswearablenotification.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.helper.WearableCommunicationHelper;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableApplicationServiceComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.ApplicationServiceModule;
import com.eightam.android.graphswearablenotification.internal.di.module.ServiceModule;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.internal.di.module.WearableModule;
import com.eightam.android.graphswearablenotification.manager.ValuesUpdateManager;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.PutDataMapRequest;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class ValuesUpdaterService extends IntentService {

    @Inject
    GoogleApiClient mGoogleApiClient;

    @Inject
    WearableCommunicationManager mWearableCommunicationManager;

    @Inject
    ValuesUpdateManager mValuesUpdateManager;

    @Inject
    ValuesStorage mValuesStorage;

    @Inject
    SettingsStorage mSettingsStorage;

    Handler mHandler;

    public ValuesUpdaterService() {
        super(ValuesUpdaterService.class.getName());
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, ValuesUpdaterService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
        initializeDi();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mValuesUpdateManager.updateValues()
                .flatMap(syncValues())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.handlerThread(mHandler))
                .subscribe(onValuesSyncedSuccess(), onValuesSyncError());
    }

    private void initialize() {
        mHandler = new Handler();
    }

    private void initializeDi() {
        DaggerWearableApplicationServiceComponent.builder()
                .applicationServiceModule(new ApplicationServiceModule(ValuesUpdaterService.this))
                .wearableModule(new WearableModule())
                .serviceModule(new ServiceModule())
                .storageModule(new StorageModule())
                .build()
                .inject(ValuesUpdaterService.this);
    }

    private boolean connectGoogleApiClientIfNeeded() {
        if (!mGoogleApiClient.isConnected()) {
            ConnectionResult connectionResult =
                    mGoogleApiClient.blockingConnect(K.app.GOOGLE_API_CONNECTION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

            return connectionResult.isSuccess();
        }
        return true;
    }

    private Func1<float[], Observable<PendingResult<DataApi.DataItemResult>>> syncValues() {
        return (values) -> {
            connectGoogleApiClientIfNeeded();

            PutDataMapRequest putDataMapRequest =
                    WearableCommunicationHelper.createSyncRequest(mValuesStorage.getValues(), mSettingsStorage.getGraphType());

            PendingResult<DataApi.DataItemResult> pendingResult =
                    mWearableCommunicationManager.putDataItem(putDataMapRequest);

            return Observable.just(pendingResult);
        };
    }

    private Action1<PendingResult<DataApi.DataItemResult>> onValuesSyncedSuccess() {
        return (pendingResult) -> {};
    }

    private Action1<Throwable> onValuesSyncError() {
        return Throwable::printStackTrace;
    }

}
