package com.eightam.android.graphswearablenotification.manager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import javax.inject.Inject;

public class WearableCommunicationManager {

    private GoogleApiClient mGoogleApiClient;

    @Inject
    public WearableCommunicationManager(GoogleApiClient googleApiClient) {
        mGoogleApiClient = googleApiClient;
    }

    public PendingResult<DataApi.DataItemResult> putDataItem(PutDataMapRequest putDataMapRequest) {
        PutDataRequest putDataRequest = putDataMapRequest.asPutDataRequest();
        return putDataItem(putDataRequest);
    }

    public PendingResult<DataApi.DataItemResult> putDataItem(PutDataRequest putDataRequest) {
        assertGoogleApiClientConnected();
        return Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest);
    }

    public PendingResult<MessageApi.SendMessageResult> sendMessage(String nodeId, String path, byte[] data) {
        assertGoogleApiClientConnected();
        return Wearable.MessageApi.sendMessage(mGoogleApiClient, nodeId, path, data);
    }

    private void assertGoogleApiClientConnected() {
        if (!mGoogleApiClient.isConnected()) {
            throw new IllegalStateException("GoogleApiClient is not connected");
        }
    }

}
