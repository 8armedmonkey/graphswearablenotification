package com.eightam.android.graphswearablenotification.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.fragment.GraphFragment;
import com.eightam.android.graphswearablenotification.service.ValuesUpdaterService;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseWearableActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    BroadcastReceiver mAppInForegroundBroadcastReceiver;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        
        menu.findItem(R.id.menu_item_settings)
                .getIcon()
                .setColorFilter(getResources().getColor(R.color.accent_color), PorterDuff.Mode.MULTIPLY);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_settings: {
                goToSettings();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(MainActivity.this);

        initializeToolbar();

        if (savedInstanceState == null) {
            showGraphs();
            requestUpdateValues();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerAppInForegroundBroadcastReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterAppInForegroundBroadcastReceiver();
    }

    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void registerAppInForegroundBroadcastReceiver() {
        if (mAppInForegroundBroadcastReceiver == null) {
            mAppInForegroundBroadcastReceiver = new AppInForegroundBroadcastReceiver();
        }

        IntentFilter filter = new IntentFilter(K.intent.action.FORWARD_BROADCAST_WITH_FOREGROUND_CHECK);
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);

        registerReceiver(mAppInForegroundBroadcastReceiver, filter);
    }

    private void unregisterAppInForegroundBroadcastReceiver() {
        if (mAppInForegroundBroadcastReceiver != null) {
            try {
                unregisterReceiver(mAppInForegroundBroadcastReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showGraphs() {
        GraphFragment graphFragment =
                (GraphFragment) getSupportFragmentManager().findFragmentByTag(K.fragment_tag.GRAPH);

        if (graphFragment == null) {
            graphFragment = GraphFragment.newInstance();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, graphFragment, K.fragment_tag.GRAPH)
                    .commit();
        }
    }

    private void requestUpdateValues() {
        startService(ValuesUpdaterService.getStartIntent(MainActivity.this));
    }

    private void goToSettings() {
        startActivity(GraphSelectionActivity.getStartIntent(MainActivity.this));
    }

    private class AppInForegroundBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            abortBroadcast();
        }

    }

}
