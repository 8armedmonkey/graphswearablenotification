package com.eightam.android.graphswearablenotification.receiver;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;

import com.eightam.android.graphswearablenotification.common.contract.Data;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.service.WearablePushHandlerService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class PushWakefulBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String payload = intent.getStringExtra(K.parse.EXTRA_DATA);

        if (!TextUtils.isEmpty(payload)) {
            JsonObject payloadAsJsonObject = getGson().fromJson(payload, JsonObject.class);

            if (payloadAsJsonObject.has(Data.key.VALUES)) {
                JsonArray valuesAsJsonArray = payloadAsJsonObject.get(Data.key.VALUES).getAsJsonArray();
                float[] values = new float[valuesAsJsonArray.size()];

                for (int i = 0, n = values.length; i < n; i++) {
                    values[i] = valuesAsJsonArray.get(i).getAsFloat();
                }

                Intent serviceIntent = WearablePushHandlerService.getStartIntent(context, values);
                startWakefulService(context, serviceIntent);
            }
        }
    }

    private Gson getGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create();
    }

}
