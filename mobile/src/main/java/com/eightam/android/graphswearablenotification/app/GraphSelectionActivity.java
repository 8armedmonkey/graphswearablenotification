package com.eightam.android.graphswearablenotification.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.common.internal.di.annotation.HasComponent;
import com.eightam.android.graphswearablenotification.constant.K;
import com.eightam.android.graphswearablenotification.fragment.GraphSelectionFragment;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableActivityComponent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageEvent;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GraphSelectionActivity extends BaseWearableActivity implements
        GraphSelectionFragment.GraphSelectionListener {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, GraphSelectionActivity.class);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

        if (K.fragment_tag.GRAPH_SELECTION.equals(fragment.getTag()) &&
                fragment instanceof GraphSelectionFragment) {
            ((GraphSelectionFragment) fragment).setGraphSelectionListener(GraphSelectionActivity.this);
        }
    }

    @Override
    public void onGraphTypeSelected(GraphType graphType) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(GraphSelectionActivity.this);

        initializeToolbar();

        if (savedInstanceState == null) {
            showGraphSelection();
        }
    }

    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void showGraphSelection() {
        GraphSelectionFragment graphSelectionFragment =
                (GraphSelectionFragment) getSupportFragmentManager().findFragmentByTag(K.fragment_tag.GRAPH_SELECTION);

        if (graphSelectionFragment == null) {
            graphSelectionFragment = GraphSelectionFragment.newInstance(GraphSelectionActivity.this);

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, graphSelectionFragment, K.fragment_tag.GRAPH_SELECTION)
                    .commit();
        }
    }

}
