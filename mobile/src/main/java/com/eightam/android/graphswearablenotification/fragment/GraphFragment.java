package com.eightam.android.graphswearablenotification.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableFragmentComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableActivityComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.widget.GraphAdapter;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GraphFragment extends BaseFragment implements
        ValuesStorage.Listener,
        SettingsStorage.Listener {

    @Inject
    ValuesStorage mValuesStorage;

    @Inject
    SettingsStorage mSettingsStorage;

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    public static GraphFragment newInstance() {
        return new GraphFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_graph, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(GraphFragment.this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDi();
    }

    @Override
    public void onStart() {
        super.onStart();
        registerStorageListeners();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(new GraphAdapter(getActivity(), mSettingsStorage.getGraphType(), mValuesStorage.getValues()));
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterStorageListeners();
    }

    @Override
    public void onValuesChanged(ValuesStorage valuesStorage) {
        ((GraphAdapter) mRecyclerView.getAdapter()).setValues(valuesStorage.getValues());
    }

    @Override
    public void onSettingsChanged(SettingsStorage settingsStorage) {
        ((GraphAdapter) mRecyclerView.getAdapter()).setGraphType(settingsStorage.getGraphType());
    }

    private void initializeDi() {
        WearableActivityComponent wearableActivityComponent = getComponent(WearableActivityComponent.class);

        DaggerWearableFragmentComponent.builder()
                .wearableActivityComponent(wearableActivityComponent)
                .storageModule(new StorageModule())
                .build()
                .inject(GraphFragment.this);
    }

    private void registerStorageListeners() {
        mValuesStorage.registerListener(GraphFragment.this);
        mSettingsStorage.registerListener(GraphFragment.this);
    }

    private void unregisterStorageListeners() {
        mValuesStorage.unregisterListener(GraphFragment.this);
        mSettingsStorage.unregisterListener(GraphFragment.this);
    }

}
