package com.eightam.android.graphswearablenotification.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eightam.android.graphs.model.GraphType;
import com.eightam.android.graphswearablenotification.R;
import com.eightam.android.graphswearablenotification.common.storage.SettingsStorage;
import com.eightam.android.graphswearablenotification.common.storage.ValuesStorage;
import com.eightam.android.graphswearablenotification.helper.WearableCommunicationHelper;
import com.eightam.android.graphswearablenotification.internal.di.component.DaggerWearableFragmentComponent;
import com.eightam.android.graphswearablenotification.internal.di.component.WearableActivityComponent;
import com.eightam.android.graphswearablenotification.internal.di.module.StorageModule;
import com.eightam.android.graphswearablenotification.manager.WearableCommunicationManager;
import com.google.android.gms.wearable.PutDataMapRequest;

import java.lang.ref.SoftReference;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GraphSelectionFragment extends BaseFragment {

    @Inject
    WearableCommunicationManager mWearableCommunicationManager;

    @Inject
    ValuesStorage mValuesStorage;

    @Inject
    SettingsStorage mSettingsStorage;

    @Bind(R.id.text_graph_selection_bubble)
    TextView mTextGraphSelectionBubble;

    @Bind(R.id.text_graph_selection_modern)
    TextView mTextGraphSelectionModern;

    @Bind(R.id.text_graph_selection_sound)
    TextView mTextGraphSelectionSound;

    @Bind(R.id.text_graph_selection_classic)
    TextView mTextGraphSelectionClassic;

    @Bind(R.id.text_graph_selection_wave)
    TextView mTextGraphSelectionWave;

    @Bind(R.id.text_graph_selection_infinite)
    TextView mTextGraphSelectionInfinite;

    SoftReference<GraphSelectionListener> mGraphSelectionListenerRef;

    public static GraphSelectionFragment newInstance(@Nullable GraphSelectionListener graphSelectionListener) {
        GraphSelectionFragment fragment = new GraphSelectionFragment();
        fragment.mGraphSelectionListenerRef = new SoftReference<>(graphSelectionListener);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_graph_selection, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(GraphSelectionFragment.this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeDi();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    public void setGraphSelectionListener(GraphSelectionListener graphSelectionListener) {
        mGraphSelectionListenerRef = new SoftReference<>(graphSelectionListener);
    }

    @OnClick({
            R.id.text_graph_selection_bubble,
            R.id.text_graph_selection_modern,
            R.id.text_graph_selection_sound,
            R.id.text_graph_selection_classic,
            R.id.text_graph_selection_wave,
            R.id.text_graph_selection_infinite})
    void selectGraphType(View v) {
        switch (v.getId()) {
            case R.id.text_graph_selection_bubble: {
                mSettingsStorage.setGraphType(GraphType.BUBBLES);
                break;
            }

            case R.id.text_graph_selection_modern: {
                mSettingsStorage.setGraphType(GraphType.MODERN);
                break;
            }

            case R.id.text_graph_selection_sound: {
                mSettingsStorage.setGraphType(GraphType.SOUND);
                break;
            }

            case R.id.text_graph_selection_classic: {
                mSettingsStorage.setGraphType(GraphType.CLASSIC);
                break;
            }

            case R.id.text_graph_selection_wave: {
                mSettingsStorage.setGraphType(GraphType.WAVE);
                break;
            }

            case R.id.text_graph_selection_infinite: {
                mSettingsStorage.setGraphType(GraphType.INFINITE);
                break;
            }
        }

        syncSettings();
        notifyGraphTypeSelected();
    }

    private void initializeDi() {
        WearableActivityComponent wearableActivityComponent = getComponent(WearableActivityComponent.class);

        DaggerWearableFragmentComponent.builder()
                .wearableActivityComponent(wearableActivityComponent)
                .storageModule(new StorageModule())
                .build()
                .inject(GraphSelectionFragment.this);
    }

    private void updateUi() {
        int selectionResourceId = R.drawable.rounded_rect_light_blue_100;

        mTextGraphSelectionBubble.setBackgroundResource(0);
        mTextGraphSelectionModern.setBackgroundResource(0);
        mTextGraphSelectionSound.setBackgroundResource(0);
        mTextGraphSelectionClassic.setBackgroundResource(0);
        mTextGraphSelectionWave.setBackgroundResource(0);
        mTextGraphSelectionInfinite.setBackgroundResource(0);

        switch (mSettingsStorage.getGraphType()) {
            case BUBBLES: {
                mTextGraphSelectionBubble.setBackgroundResource(selectionResourceId);
                break;
            }

            case MODERN: {
                mTextGraphSelectionModern.setBackgroundResource(selectionResourceId);
                break;
            }

            case SOUND: {
                mTextGraphSelectionSound.setBackgroundResource(selectionResourceId);
                break;
            }

            case CLASSIC: {
                mTextGraphSelectionClassic.setBackgroundResource(selectionResourceId);
                break;
            }

            case WAVE: {
                mTextGraphSelectionWave.setBackgroundResource(selectionResourceId);
                break;
            }

            case INFINITE: {
                mTextGraphSelectionInfinite.setBackgroundResource(selectionResourceId);
                break;
            }
        }
    }

    private void syncSettings() {
        PutDataMapRequest putDataMapRequest =
                WearableCommunicationHelper.createSyncRequest(mValuesStorage.getValues(), mSettingsStorage.getGraphType());

        mWearableCommunicationManager.putDataItem(putDataMapRequest);
    }

    private void notifyGraphTypeSelected() {
        if (mGraphSelectionListenerRef != null) {
            GraphSelectionListener graphSelectionListener = mGraphSelectionListenerRef.get();

            if (graphSelectionListener != null) {
                graphSelectionListener.onGraphTypeSelected(mSettingsStorage.getGraphType());
            }
        }
    }

    public interface GraphSelectionListener {

        void onGraphTypeSelected(GraphType graphType);

    }

}
