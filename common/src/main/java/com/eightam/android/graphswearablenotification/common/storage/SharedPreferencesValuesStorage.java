package com.eightam.android.graphswearablenotification.common.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.ref.SoftReference;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.inject.Inject;

public class SharedPreferencesValuesStorage implements ValuesStorage {

    private static final String PREFS_NAME = "valuesStorage";
    private static final String KEY_VALUES_JSON = "valuesJson";

    private SharedPreferences mSharedPreferences;
    private Gson mGson;
    private Map<Listener, SharedPreferences.OnSharedPreferenceChangeListener> mListeners;

    @Inject
    public SharedPreferencesValuesStorage(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mGson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
        mListeners = new WeakHashMap<>();
    }

    @Override
    public float[] getValues() {
        String valuesAsJsonString = mSharedPreferences.getString(KEY_VALUES_JSON, null);

        if (!TextUtils.isEmpty(valuesAsJsonString)) {
            Type listType = new TypeToken<List<Float>>(){}.getType();

            List<Float> values = mGson.fromJson(valuesAsJsonString, listType);
            float[] valuesAsPrimitives = new float[values.size()];

            for (int i = 0, n = values.size(); i < n; i++) {
                valuesAsPrimitives[i] = values.get(i);
            }
            return valuesAsPrimitives;
        }
        return new float[0];
    }

    @Override
    public void setValues(float[] values) {
        String valuesAsJsonString = mGson.toJson(values);

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_VALUES_JSON, valuesAsJsonString).apply();
    }

    @Override
    public void registerListener(Listener listener) {
        SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener =
                new SharedPreferencesValuesStorageListener(SharedPreferencesValuesStorage.this, listener);

        mSharedPreferences.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
        mListeners.put(listener, onSharedPreferenceChangeListener);
    }

    @Override
    public void unregisterListener(Listener listener) {
        SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener = mListeners.get(listener);

        if (onSharedPreferenceChangeListener != null) {
            mSharedPreferences.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
        }
    }

    private static class SharedPreferencesValuesStorageListener implements SharedPreferences.OnSharedPreferenceChangeListener {

        ValuesStorage mValuesStorage;
        SoftReference<Listener> mListenerRef;

        public SharedPreferencesValuesStorageListener(ValuesStorage valuesStorage, Listener listener) {
            mValuesStorage = valuesStorage;
            mListenerRef = new SoftReference<>(listener);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Listener listener = mListenerRef.get();

            if (listener != null) {
                listener.onValuesChanged(mValuesStorage);
            }
        }

    }

}
