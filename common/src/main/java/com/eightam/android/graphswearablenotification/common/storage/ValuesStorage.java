package com.eightam.android.graphswearablenotification.common.storage;

public interface ValuesStorage {

    float[] getValues();

    void setValues(float[] values);

    void registerListener(Listener listener);

    void unregisterListener(Listener listener);

    interface Listener {

        void onValuesChanged(ValuesStorage valuesStorage);

    }

}
