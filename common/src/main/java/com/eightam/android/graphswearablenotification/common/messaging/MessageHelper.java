package com.eightam.android.graphswearablenotification.common.messaging;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class MessageHelper {

    public static MessageData toMessageData(byte[] bytes) {
        Gson gson = getGson();
        return gson.fromJson(new String(bytes), MessageData.class);
    }

    public static byte[] toBytes(MessageData messageData) {
        Gson gson = getGson();
        Reader reader = new StringReader(gson.toJson(messageData));
        byte[] bytes = new byte[0];

        try {
            bytes = IOUtils.toByteArray(reader);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(reader);
        }
        return bytes;
    }

    private static Gson getGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create();
    }

}
