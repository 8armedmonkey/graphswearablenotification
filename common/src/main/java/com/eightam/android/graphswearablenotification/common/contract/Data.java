package com.eightam.android.graphswearablenotification.common.contract;

public interface Data {

    interface path {

        String SYNC = "/sync";
        String NODE = "/node";

    }

    interface action {

        String SYNC = "sync";
        String WAKE_UP = "wakeUp";
    }

    interface key {

        String ACTION = "action";
        String VALUES = "values";
        String GRAPH_TYPE = "graphType";

    }

    interface capability {

        String SYNC = "sync";

    }

}
