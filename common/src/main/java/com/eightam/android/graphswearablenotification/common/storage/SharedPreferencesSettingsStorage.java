package com.eightam.android.graphswearablenotification.common.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.eightam.android.graphs.model.GraphType;

import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.WeakHashMap;

import javax.inject.Inject;

public class SharedPreferencesSettingsStorage implements SettingsStorage {

    private static final String PREFS_NAME = "settingsStorage";
    private static final String KEY_GRAPH_TYPE = "graphType";

    private SharedPreferences mSharedPreferences;
    private Map<Listener, SharedPreferences.OnSharedPreferenceChangeListener> mListeners;

    @Inject
    public SharedPreferencesSettingsStorage(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mListeners = new WeakHashMap<>();
    }

    @Override
    public GraphType getGraphType() {
        String graphTypeAsString = mSharedPreferences.getString(KEY_GRAPH_TYPE, null);

        if (GraphType.BUBBLES.toString().equals(graphTypeAsString)) {
            return GraphType.BUBBLES;

        } else if (GraphType.MODERN.toString().equals(graphTypeAsString)) {
            return GraphType.MODERN;

        } else if (GraphType.SOUND.toString().equals(graphTypeAsString)) {
            return GraphType.SOUND;

        } else if (GraphType.CLASSIC.toString().equals(graphTypeAsString)) {
            return GraphType.CLASSIC;

        } else if (GraphType.WAVE.toString().equals(graphTypeAsString)) {
            return GraphType.WAVE;

        } else if (GraphType.INFINITE.toString().equals(graphTypeAsString)) {
            return GraphType.INFINITE;

        }
        return GraphType.BUBBLES;
    }

    @Override
    public void setGraphType(GraphType graphType) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_GRAPH_TYPE, graphType.toString()).apply();
    }

    @Override
    public void registerListener(Listener listener) {
        SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener =
                new SharedPreferencesSettingsStorageListener(SharedPreferencesSettingsStorage.this, listener);

        mSharedPreferences.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
        mListeners.put(listener, onSharedPreferenceChangeListener);
    }

    @Override
    public void unregisterListener(Listener listener) {
        SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener = mListeners.get(listener);

        if (onSharedPreferenceChangeListener != null) {
            mSharedPreferences.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
        }
    }

    private static class SharedPreferencesSettingsStorageListener implements SharedPreferences.OnSharedPreferenceChangeListener {

        SettingsStorage mSettingsStorage;
        SoftReference<Listener> mListenerRef;

        public SharedPreferencesSettingsStorageListener(SettingsStorage settingsStorage, Listener listener) {
            mSettingsStorage = settingsStorage;
            mListenerRef = new SoftReference<>(listener);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Listener listener = mListenerRef.get();

            if (listener != null) {
                listener.onSettingsChanged(mSettingsStorage);
            }
        }
    }

}
