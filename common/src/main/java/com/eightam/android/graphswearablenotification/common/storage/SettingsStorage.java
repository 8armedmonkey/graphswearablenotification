package com.eightam.android.graphswearablenotification.common.storage;

import com.eightam.android.graphs.model.GraphType;

public interface SettingsStorage {

    GraphType getGraphType();

    void setGraphType(GraphType graphType);

    void registerListener(Listener listener);

    void unregisterListener(Listener listener);

    interface Listener {

        void onSettingsChanged(SettingsStorage settingsStorage);

    }

}
