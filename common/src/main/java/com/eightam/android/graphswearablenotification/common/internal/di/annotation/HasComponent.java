package com.eightam.android.graphswearablenotification.common.internal.di.annotation;

public interface HasComponent<C> {

    C getComponent();

}
