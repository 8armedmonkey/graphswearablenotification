package com.eightam.android.graphswearablenotification.common.messaging;

import java.util.HashMap;
import java.util.Map;

public class MessageData {

    private String mAction;
    private Map<String, Object> mKeyValueMap;

    public MessageData() {
        mKeyValueMap = new HashMap<>();
    }

    public String getAction() {
        return mAction;
    }

    public void setAction(String action) {
        mAction = action;
    }

    public Map<String, Object> getKeyValueMap() {
        return mKeyValueMap;
    }

    public void setKeyValueMap(Map<String, Object> keyValueMap) {
        mKeyValueMap = keyValueMap;
    }

}
