package com.eightam.service;

import java.util.List;

import rx.Observable;

public interface ValuesService {

    Observable<List<Float>> getValuesStream();

}
