package com.eightam.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import rx.Observable;

public class RandomValuesService implements ValuesService {

    private int mNumOfValues;
    private float mMinValue;
    private float mMaxValue;
    private Random mRandom;

    public RandomValuesService(int numOfValues, float minValue, float maxValue) {
        mNumOfValues = numOfValues;
        mMinValue = minValue;
        mMaxValue = maxValue;
        mRandom = new Random(System.currentTimeMillis());
    }

    @Override
    public Observable<List<Float>> getValuesStream() {
        List<Float> values = new ArrayList<Float>();
        float range = mMaxValue - mMinValue;

        for (int i = 0; i < mNumOfValues; i++) {
            values.add(mRandom.nextFloat() * range + mMinValue);
        }
        return Observable.just(values);
    }

}
